<?php

require_once "DefaultRepository.php";
require_once dirname(__DIR__) . "/Model/User.php";

class UserRepository extends DefaultRepository
{
    public function createUser(string $name, string $email, string $password, int $phone)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO USERS (NAME, PASSWORD, EMAIL, PHONE)
            VALUES (?, ?, ?, ?);
            ');
        $password_hased = password_hash($password, PASSWORD_DEFAULT);
        $stmt->execute([$name, $password_hased, $email, $phone]);
    }

    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM USERS WHERE EMAIL = ?
        ');
        $stmt->execute([$email]);

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false) {
            return null;
        }

        return new User(
            $user['NAME'],
            $user['PASSWORD'],
            $user['EMAIL'],
            $user['PHONE'],
            $user['ROLE'],
            $user['ID'],
        );
    }

    public function updateUser($name, $email, $id)
    {
        $stmt = $this->database->connect()->prepare('UPDATE USERS
        SET NAME  = (?),
            EMAIL = (?)
        WHERE ID = (?)');

        if ($stmt->execute([$name, $email, $id])) {
            $_SESSION['name'] = $name;
        }
    }
}