<?php

require_once "Database.php";

class DefaultRepository
{
    protected Database $database;

    public function __construct()
    {
        $this->database = new Database();
    }
}