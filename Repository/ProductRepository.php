<?php

require_once "DefaultRepository.php";
require_once dirname(__DIR__) . "/Model/Product.php";

#Todo: Test inserting/fetching barcodes

class ProductRepository extends DefaultRepository
{

    public function CreateProduct(string $name, int $creator, int $container_id, string $manufacturer, array $barcodes)
    {
        $stmt = $this->database->connect();
        $stmt->prepare('
        INSERT INTO PRODUCTS (NAME, CONTAINER_ID, CREATOR, MANUFACTURER) VALUES (?,?,?,?)
        ')->execute([$name, $container_id, $creator, $manufacturer]);


        $stmt2 = $this->database->connect();
        foreach ($barcodes as $barcode) {
            $stmt2->prepare('
        INSERT INTO PRODUCTS_BARCODES (ID_PRODUCT, BARCODE) VALUES (?, ?)
        ')->execute([$stmt->lastInsertId(), $barcode]);
        }
    }

    private function getBarcodesByID(int $product_id): array
    {
        $connect_statement = $this->database->connect();
        $barcode_stmt = $connect_statement->prepare('
        SELECT BARCODE FROM PRODUCTS_BARCODES WHERE ID_PRODUCT = (?)
        ');
        $barcode_stmt->execute([$product_id]);

        return $barcode_stmt->fetchAll(PDO::FETCH_COLUMN);
    }

    public function getProductByID(int $product_id)
    {
        $connect_statement = $this->database->connect();

        $product_stmt = $connect_statement->prepare('
        SELECT * FROM PRODUCTS WHERE ID = (?)
        ');
        $product_stmt->execute([$product_id]);
        $product = $product_stmt->fetch(PDO::FETCH_ASSOC);

        $barcodes = $this->getBarcodesByID($product_id);

        if ($product == false or $barcodes == false) {
            return null;
        }

        return new Product(
            $product['NAME'],
            $product['CREATOR'],
            $product['CONTAINER_ID'],
            $product['MANUFACTURER'],
            $barcodes,
            $product['ID']
        );
    }

    public function getProductByCreator(int $creator): ?Product
    {
        $connect_statement = $this->database->connect();

        $product_stmt = $connect_statement->prepare('
        SELECT * FROM PRODUCTS WHERE CREATOR = (?)
        ');
        $product_stmt->execute([$creator]);
        $product = $product_stmt->fetch(PDO::FETCH_ASSOC);

        $barcodes = $this->getBarcodesByID((int)[$product['ID']]);

        if ($product == false or $barcodes == false) {
            return null;
        }

        return new Product(
            $product['NAME'],
            $product['CREATOR'],
            $product['CONTAINER_ID'],
            $product['MANUFACTURER'],
            $barcodes,
            $product['ID']
        );
    }

}