<?php

require_once 'Controllers\MainPageController.php';
require_once 'Controllers\SecurityController.php';
require_once 'Controllers\ProductController.php';
require_once 'Controllers\UserController.php';
class Routing
{
    private array $routes;

    public function __construct()
    {
        $this->routes = [
            'mainpage' => [
                'controller' => 'MainPageController',
                'action' => 'show'
            ],
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'register' => [
                'controller' => 'SecurityController',
                'action' => 'register'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'product' => [
                'controller' => 'ProductController',
                'action' => 'get'
            ],
            'addproduct' => [
                'controller' => 'ProductController',
                'action' => 'addproduct'
            ],
            'searchproduct' => [
                'controller' => 'ProductController',
                'action' => 'searchproduct'
            ],
            'profile' => [
                'controller' => 'UserController',
                'action' => 'show'
            ]

        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'addproduct';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}