# Projekt aplikacji Jak wyrzucę /W trakcie realizacji #
Aplikacja internetowa będzie odpowiedzią na realne problemy społeczeństwa wynikające z obowiązku segregacji odpadów. Przeznaczona dla każdego niezależnie od wieku. Aplikacja będzie miała na celu wskazanie użytkownikom gdzie należy daną rzecz wyrzucić ( konkretnie do którego pojemnika ), będzie wyposażona w wyszukiwarkę umożliwiającą wyszukiwanie po nazwie oraz kodzie kreskowym produktu. Dane będą przechowywane w bazie danych, która powiększana będzie przy pomocy użytkowników - idea ‘crowdsourcingu’. 

### Database config
W pliku `php.ini` włącz obsługę MYSQL. Utwórz plik `/DB_config.php` o poniższej zawartości i wypełnij pola zgodnie ze swoją bazą danych.

    <?php
      const USERNAME = "";
      const PASSWORD = "";
      const DATABASE = "";
      const HOST = "";