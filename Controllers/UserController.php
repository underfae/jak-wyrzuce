<?php
require_once 'AppController.php';

class UserController extends AppController
{
    public function show()
    {
        $this->render('profile');
    }
}