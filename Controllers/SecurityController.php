<?php
require_once 'AppController.php';
require_once dirname(__DIR__) . "/Repository/UserRepository.php";

class SecurityController extends AppController
{
    public function login()
    {
        if ($this->isPost()) {
            $userRepository = new UserRepository();

            $email = $_POST['email'];
            $password = $_POST['password'];
            $user = $userRepository->getUser($email);
            if (!$user) {
                $this->render('login', ['messages' => ['Użytkownik z takim adresem email nie istnieje!']]);
                return;
            } elseif (!password_verify($password, $user->getPassword())) {
                $this->render('login', ['messages' => ['Podane hasło jest błędne!']]);
                return;
            }

            $_SESSION["id"] = $user->getId();
            $_SESSION["email"] = $user->getEmail();
            $_SESSION["name"] = $user->getName();
            $_SESSION["role"] = $user->getRole();
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=mainpage");
            return;
        }

        $this->render('login');
    }

    public function register()
    {
        if ($this->isPost()) {
            $userRepository = new UserRepository();

            $name = $_POST['name_surname'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $phone = $_POST['phone_number'];
            $user = $userRepository->getUser($email);

            if ($user) {
                die("Użytkownik z takim adresem email już istnieje");
            }

            $user = $userRepository->createUser($name, $email, $password, $phone);
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=login");
            return;
        }

        $this->render('register');
    }

    public function logout(): void
    {
        session_unset();
        session_destroy();

        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}?page=login");
        return;
    }
}