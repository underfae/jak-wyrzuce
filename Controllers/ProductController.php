<?php

include_once "AppController.php";
include_once dirname(__DIR__) . "/Repository/ProductRepository.php";


class ProductController extends AppController
{
    private function error_msg(string $error_string, ?int $id = null): void
    {
        if ($id) {
            echo json_encode(["id" => $id, "Error_msg" => $error_string]);
        } else {
            echo json_encode(["Error_msg" => $error_string]);
        }
    }

    public function get()
    {
        header('Content-Type: application/json');
        if ($this->isGet()) {
            if (isset($_GET['id'])) {

                $products = new ProductRepository();

                echo "[";
                foreach (json_decode($_GET['id']) as $id) {
                    if (!is_int($id)) {
                        $this->error_msg("Wrong id", $id);
                        continue;
                    }
                    $product = $products->getProductByID($id);

                    if ($product) {
                        echo json_encode($product);
                    } else {
                        $this->error_msg("Product not found", $id);
                    }
                    echo ",";
                }
                echo "]";
            } elseif (isset($_GET['creator'])) {
                $products = new ProductRepository();
                $products = $products->getProductByCreator($_GET['creator']);
                if ($products) {
                    echo json_encode($products);
                } else {
                    $this->error_msg("This user haven't created any product");
                }
            }
        } else {
            $this->error_msg("Missing 'id' or 'creator' parameter");
        }
    }

    public function addproduct()
    {
        #Todo: Finish this, add 'icons' or sth to the DB
        if ($this->isPost()) {
            $name = $_GET['item-name'];
            $manufacturer = $_GET['company-name'];
            $barcode = $_GET['bar-code'];
            $container_id = $_POST['icons'];
            foreach ($container_id as $container) {
                echo $container . "<p>";
            }
            die();

            $productRepo = new ProductRepository();
            $productRepo->CreateProduct($name, $_SESSION["id"], $container_id, $manufacturer, $barcode);
        }

        $this->render('additem');
    }
    public function searchproduct() {
        $this->render('search');
    }
}