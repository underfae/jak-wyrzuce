<?php


class Product implements JsonSerializable
{
    private ?int $id;
    private string $name;
    private int $creator;
    private string $container_id;
    private string $manufacturer;
    private ?array $barcodes;

    public function __construct(string $name, int $creator, int $container_id, string $manufacturer, ?array $barcodes = null, ?int $id = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->creator = $creator;
        $this->container_id = $container_id;
        $this->manufacturer = $manufacturer;
        $this->barcodes = $barcodes;
    }

    public function JsonSerialize(): array
    {
        return get_object_vars($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCreator(): int
    {
        return $this->creator;
    }

    public function getContainerid(): int
    {
        return $this->container_id;
    }

    public function getManufcaturer(): string
    {
        return $this->manufacturer;
    }

    public function getBarcodes(): array
    {
        return $this->barcodes;
    }

}