<?php

class User
{
    private ?int $id;
    private string $name;
    private string $password;
    private string $email;
    private int $phone;
    private ?int $role;

    public function __construct(string $name, string $password, string $email, int $phone, ?int $role = null, ?int $id = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->password = $password;
        $this->email = $email;
        $this->phone = $phone;
        $this->role = $role;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRole(): ?int
    {
        return $this->role;
    }

}