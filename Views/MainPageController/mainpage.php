<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include("Views/Common/headings.php") ?>
    <link rel="stylesheet" type="text/css" href="../../Public/css/mainpage.css">
    <link rel="stylesheet" type="text/css" href="../../Public/css/mainlayout.css">
    <title>Strona Głowna</title>
</head>
<body>
<?php include("Views/Common/header.php") ?>
<div class="wrapper">
    <?php include("Views/Common/navbar.php") ?>
    <section class="latest">
        <label class="section_name"> OSTATNIO DODANE </label>
        <?php foreach ($latest_results as $result): ?>
            <div class="box">
                <div class="pic">
                    <img src=" <?= $result->getImage() ?>">
                </div>
                <div class="bar">
                    <div class="icons">
                        <span class="likes"><i class="far fa-heart"></i>  <?= $result->getLikes() ?></span>
                        <span class="comments"> <i class="far fa-comment"></i> <?= $result->getComments() ?></span>
                    </div>
                    <div class="name">
                        <label class="company">  <?= $result->getCompanyName() ?> </label>
                        <label class="item">  <?= $result->getItemName() ?> </label>
                    </div>
                </div>
            </div>
        <?php endforeach ?>

        <div class="label">
            <label> Wyświetl więcej</label>
            <i class="far fa-arrow-alt-circle-right"></i>
        </div>
    </section>

    <section class="yours">
        <label class="section_name"> DODANE PRZEZ CIEBIE </label>
        <?php foreach ($yours_results as $result): ?>
            <div class="box">
                <div class="pic">
                    <img src=" <?= $result->getImage() ?>">
                </div>
                <div class="bar">
                    <div class="icons">
                        <span class="likes"><i class="far fa-heart"></i>  <?= $result->getLikes() ?></span>
                        <span class="comments"> <i class="far fa-comment"></i> <?= $result->getComments() ?></span>
                    </div>
                    <div class="name">
                        <label class="company">  <?= $result->getCompanyName() ?> </label>
                        <label class="item">  <?= $result->getItemName() ?> </label>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
        <div class="label">
            <label> Wyświetl więcej</label>
            <i class="far fa-arrow-alt-circle-right"></i>
        </div>
    </section>
</div>
</body>
</html>





