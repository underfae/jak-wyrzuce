<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("Views/Common/headings.php") ?>
    <link rel="stylesheet" type="text/css" href="../../Public/css/profile.css">
    <link rel="stylesheet" type="text/css" href="../../Public/css/mainlayout.css">
    <script src="../../Public/js/profile.js"></script>
    <title> Twój profil </title>
</head>
<body>
<?php include("Views/Common/header.php") ?>
<div class="wrapper">
    <?php include("Views/Common/navbar.php") ?>
    <div class="frame">
        <div id="main_menu">

            <div class="profile-info">
                <img src="../../Public/img/sok.jpg">

                <div class="info">
                    <label class="name"> Gabriela Frączek </label>
                    <label class="email"> gfraczek1999@gmail.com </label>
                </div>

                <div class="numbers">

                    <div class="numberof">
                        <label> 45 </label>
                        <label> DODANE </label>
                    </div>
                    <hr>
                    <div class="numberof">
                        <label> 56 </label>
                        <label> POLUBIONE </label>
                    </div>
                    <hr>
                    <div class="numberof">
                        <label> 76 </label>
                        <label> KOMENTARZE</label>
                    </div>
                </div>

            </div>
            <div class="submenu">
                <ul>
                    <li>
                        <button onclick="show_settings()"> USTAWIENIA</button>
                    </li>
                    <li>
                        <button onclick="show_feedback()"> WYŚLIJ OPINIĘ</button>
                    </li>
                    <li>
                        <button onclick="show_policy()"> POLITYKA PRYWATNOŚCI</button>
                    </li>
                    <li>
                        <a href="?page=logout"> <button>WYLOGUJ</button></a>
                    </li>
                </ul>
            </div>
        </div>

        <div id="line"></div>
        <div id="settings">

        </div>
        <div id="feedback">
            <label class="mainlabel">DODAJ SWOJĄ OPINIĘ</label>
            <form>
                <label for="name">Imie </label>
                <input type="text" id="name" name="name" placeholder="Twoje imie...">
                <label for="email">E-mail </label>
                <input type="email" id="email" name="email" placeholder="Twój email...">
                <label for="opinion"> Twoja opinia</label>
                <textarea id="opinion" name="opinion" placeholder="Napisz coś..."></textarea>
                <button type="submit">Wyślij</button>
            </form>
        </div>
        <div id="policy">
            <iframe src="../../Public/others/policy.pdf" style="width:100%;height:100%;"></iframe>
        </div>


    </div>

</div>
</body>
</html>