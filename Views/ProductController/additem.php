<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("Views/Common/headings.php") ?>
    <link rel="stylesheet" type="text/css" href="../../Public/css/additem.css">
    <link rel="stylesheet" type="text/css" href="../../Public/css/mainlayout.css">
    <script src="../../Public/js/forms.js"></script>
    <title> Dodaj produkt </title>
</head>
<body>
<?php include("Views/Common/header.php") ?>
<div class="wrapper">
    <?php include("Views/Common/navbar.php") ?>
    <div class="frame">
        <form method="post">
            <div class="basic-info">
                <div class="image">
                    <img id="output"/>
                    <label for="file" id="filelabel"> Wybierz zdjęcie</label>
                    <input type="file" accept="image/*" name="image" id="file" onchange="loadFile(event)">
                </div>
                <div class="input-info">
                    <input name="item-name" type="text" placeholder="Nazwa produktu">
                    <input name="company-name" type="text" placeholder="Firma">
                    <input name="bar-code" type="text" placeholder="Kod kreskowy" maxlength="9">
                </div>
            </div>

            <div class="checkboxes">
                <label class="title"> Wybierz jakie oznaczenia występują na opakowaniu</label>
                <div class="icons">
                    <input type="checkbox" name="icons[]" value="1" id="Checkbox1"/>
                    <label for="Checkbox1"><img src="../../Public/img/icons/1.png"/></label>
                    <input type="checkbox" name="icons[]" value="2" id="Checkbox2"/>
                    <label for="Checkbox2"><img src="../../Public/img/icons/2.png"/></label>
                    <input type="checkbox" name="icons[]" value="3" id="Checkbox3"/>
                    <label for="Checkbox3"><img src="../../Public/img/icons/3.png"/></label>
                    <input type="checkbox" name="icons[]" value="4" id="Checkbox4"/>
                    <label for="Checkbox4"><img src="../../Public/img/icons/4.png"/></label>
                    <input type="checkbox" name="icons[]" value="5" id="Checkbox5"/>
                    <label for="Checkbox5"><img src="../../Public/img/icons/5.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox6"/>
                    <label for="Checkbox6"><img src="../../Public/img/icons/6.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox7"/>
                    <label for="Checkbox7"><img src="../../Public/img/icons/7.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox8"/>
                    <label for="Checkbox8"><img src="../../Public/img/icons/20.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox9"/>
                    <label for="Checkbox9"><img src="../../Public/img/icons/21.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox10"/>
                    <label for="Checkbox10"><img src="../../Public/img/icons/22.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox11"/>
                    <label for="Checkbox11"><img src="../../Public/img/icons/40.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox12"/>
                    <label for="Checkbox12"><img src="../../Public/img/icons/41.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox13"/>
                    <label for="Checkbox13"><img src="../../Public/img/icons/70.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox14"/>
                    <label for="Checkbox14"><img src="../../Public/img/icons/71.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox15"/>
                    <label for="Checkbox15"><img src="../../Public/img/icons/72.png"/></label>
                    <input type="checkbox" name="icons[]" id="Checkbox16"/>
                    <label for="Checkbox16"><img src="../../Public/img/icons/alu.jpg"/></label>
                </div>
                <div class="buttons">
                    <button type="submit"> Dodaj opakowanie</button>
                    <button type="button"> Inne oznaczenie</button>
                </div>
            </div>
            <!--            <label> Jeżeli masz problem z wyborem kliknij <a href="https://tiny.pl/7x32g" target="_blank"> tutaj</a>-->
            <!--            </label>-->
        </form>
    </div>
</div>
</body>
</html>