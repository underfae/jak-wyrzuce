<!DOCTYPE html>
<head>
    <?php include("Views/Common/headings.php") ?>
    <link rel="stylesheet" type="text/css" href="../../Public/css/mainlayout.css">
    <link rel="stylesheet" type="text/css" href="../../Public/css/search.css">
    <title> Wyszukaj produkt </title>
</head>
<body>
<?php include("Views/Common/header.php") ?>
<div class="wrapper">
    <?php include("Views/Common/navbar.php") ?>
    <div class="frame">
        <div class="search-input">
            <div class="info"> Podaj informację na temat interesującego cię opakowania</div>
            <form method="post">
                <input name="item-name" type="text" placeholder="Nazwa produktu">
                <input name="company-name" type="text" placeholder="Firma">
                <input name="bar-code" type="text" placeholder="Kod kreskowy" maxlength="9">
                <button type="submit"> Szukaj</button>
            </form>
            <div class="add-info">
                <label> Brak wyników? </label>
                <button type="button"> Dodaj opakowanie</button>
            </div>
        </div>
        <div class="line"></div>
        <div class="results">
            <?php foreach ($results as $result): ?>
                <div class="box">
                    <div class="pic">
                        <img src=" <?= $result->getImage() ?>">
                    </div>
                    <div class="bar">
                        <div class="icons">
                            <span class="likes"><i class="far fa-heart"></i>  <?= $result->getLikes() ?></span>
                            <span class="comments"> <i class="far fa-comment"></i> <?= $result->getComments() ?></span>
                        </div>
                        <div class="name">
                            <label class="company">  <?= $result->getCompanyName() ?> </label>
                            <label class="item">  <?= $result->getItemName() ?> </label>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>

        </div>
    </div>

</div>
</body>
</html>
