<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include("Views/Common/headings.php") ?>
    <link rel="stylesheet" type="text/css" href="../../Public/css/login.css">
    <title>Jak wyrzucę</title>
</head>
<body>
<div class="container">
    <div class="logo">
        <img src="../../Public/img/logo.png" alt="Błąd ładowania zdjęcia">
        <label class="logo_name"> Jak wyrzucę</label>
    </div>
    <div class="login-form">
        <label class="login_label">Zaloguj się</label>
        <label class="login_error_info">
            <?php
            if (isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }
            }
            ?>
        </label>
        <form method="post" action="?page=login">
            <input name="email" type="email" placeholder="gfraczek123@op.pl">
            <input name="password" type="password" placeholder="•••••••">
            <button type="submit">
                <i class="fas fa-arrow-right"></i></button>
        </form>
        <button class="facebook" type="button">
            <i class="fab fa-facebook-square"></i>ZALOGUJ SIĘ PRZEZ FACEBOOKA
        </button>
        <a href="?page=register">NIE MASZ KONTA? ZAREJESTRUJ SIĘ</a>
    </div>
</div>
</body>
</html>