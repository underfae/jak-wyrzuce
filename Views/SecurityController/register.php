<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include("Views/Common/headings.php") ?>
    <link rel="stylesheet" type="text/css" href="../../Public/css/register.css">
    <title>Zarejestruj się</title>
</head>
<body>
<div class="container">
    <div class="left-side">
        <div class="logo">
            <img alt="Logo" src="../../Public/img/logo.png">
            <label class="logo_name"> Utwórz konto</label>
        </div>
        <div class="welcome_text">
            Wypełnij formularz i dołącz się do wspólnego dbania o środowisko.
        </div>
    </div>
    <div class="right-side">
        <form method="post" action="?page=register">
            <label for="name_surname">Imię i Nazwisko</label>
            <input id="name_surname" name="name_surname" type="text" placeholder="Gabriela Frączek">
            <label for="email">Email</label>
            <input id="email" name="email" type="email" placeholder="gfraczek123@op.pl">
            <label for="password">Hasło</label>
            <input id="password" name="password" type="password" placeholder="•••••••">
            <label for="phone_number">Numer telefonu</label>
            <input id="phone_number" name="phone_number" type="text" maxlength="9" placeholder="731990351">
            <button class="continue" type="submit"> KONTYNUUJ</button>
        </form>
    </div>
</div>
</body>
</html>