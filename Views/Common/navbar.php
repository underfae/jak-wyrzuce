<ul id="navigation">
    <li class="mainpage"><a href="/" title="Strona Główna"><label>Strona Główna</label><i class="fas fa-home"></i></a></li>
    <li class="notifications"><a href="" title="Powiadomienia"><label>Powiadomienia</label><i class="fas fa-bell"></i></a></li>
    <li class="search"><a href="?page=searchproduct" title="Wyszukiwarka"><label>Szukaj opakowania</label><i class="fas fa-search"></i></a></li>
    <li class="add"><a href="?page=addproduct" title="Dodawanie opakowania"><label>Dodaj opakowanie</label><i class="fas fa-plus"></i></a></li>
    <?php if ($_SESSION) { ?>
        <li class="profile"><a href="?page=profile" title="Twój profil"><label>Twój profil</label><i class="far fa-user-circle"></i></a></li>
        <li class="logout"><a href="?page=logout" title="Wyloguj się"><label> Wyloguj się</label><i class="fas fa-sign-out-alt"></i></a></li>
        <?php if ($_SESSION["role"]) { ?>
            <li class="adminpanel"><a href="" title="Panel admina"><label>Panel Admina</label><i class="fas fa-users-cog"></i></i></a></li>
        <?php }
    } else { ?>
        <li class="login"><a href="?page=login" title="Zaloguj"><label>Zaloguj się</label><i class="far fa-user-circle"></i></a></li>
    <?php } ?>
</ul>