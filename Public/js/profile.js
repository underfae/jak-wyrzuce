function show_feedback() {
    document.getElementById("policy").style.display = "none";
    document.getElementById("settings").style.display = "none";
    document.getElementById("feedback").style.display = "flex";
}

function show_settings() {
    document.getElementById("policy").style.display = "none";
    document.getElementById("feedback").style.display = "none";
    document.getElementById("settings").style.display = "flex";
}

function show_policy() {
    document.getElementById("settings").style.display = "none";
    document.getElementById("feedback").style.display = "none";
    document.getElementById("policy").style.display = "flex";
}