<?php

include "DB_config.php";

class Database
{
    private string $username;
    private string $password;
    private string $database;
    private string $host;

    private ?PDO $connection_handle;

    function __construct()
    {
        $this->username = USERNAME;
        $this->password = PASSWORD;
        $this->database = DATABASE;
        $this->host = HOST;
    }

    public function connect(): ?PDO
    {
        try {
            $options = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            );
            $this->connection_handle = new PDO("mysql:host=$this->host;dbname=$this->database", $this->username, $this->password, $options);
            return $this->connection_handle;
        } catch (PDOException $e) {
            echo "Błąd łączenia z bazą danych\n";
            return null;
        }
    }

    public function close()
    {
        $this->connection_handle = null;
    }

}