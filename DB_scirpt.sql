create table CONTAINER
(
    ID   int         not null,
    NAME varchar(11) null,
    constraint Container_ID_uindex
        unique (ID)
);

alter table CONTAINER
    add primary key (ID);

create table `CONTAINER/ICON TYPE`
(
    ID        int not null,
    TYPE_ICON int null,
    constraint table_name_ID_uindex
        unique (ID)
);

alter table `CONTAINER/ICON TYPE`
    add primary key (ID);

create table RIC
(
    ID   int not null,
    NAME int null,
    constraint RIC_ID_uindex
        unique (ID)
);

alter table RIC
    add primary key (ID);

create table USERS
(
    ID       int auto_increment,
    NAME     varchar(32) null,
    PASSWORD varchar(60) null,
    EMAIL    varchar(64) null,
    PHONE    int(9)      null,
    constraint USERS_ID_uindex
        unique (ID)
);

alter table USERS
    add primary key (ID);

create table PRODUCTS
(
    ID           int auto_increment,
    NAME         varchar(32) null,
    CREATOR      int         null,
    CONTAINER_ID int         null,
    MANUFACTURER varchar(32) null,
    constraint PRODUCTS_ID_uindex
        unique (ID),
    constraint PRODUCTS_CONTAINER_ID_fk
        foreign key (CONTAINER_ID) references CONTAINER (ID),
    constraint PRODUCTS_USERS_ID_fk
        foreign key (CREATOR) references USERS (ID)
);

create index PRODUCTS_RIC_ID_fk
    on PRODUCTS (CONTAINER_ID);

alter table PRODUCTS
    add primary key (ID);

create table IMAGES
(
    ID         int auto_increment,
    ID_PRODUCT int          null,
    FILE_NAME  varchar(200) null,
    constraint IMAGES_ID_uindex
        unique (ID),
    constraint IMAGES_PRODUCTS_ID_fk
        foreign key (ID_PRODUCT) references PRODUCTS (ID)
);

alter table IMAGES
    add primary key (ID);

create table PRODUCTS_BARCODES
(
    ID         int auto_increment,
    ID_PRODUCT int null,
    BARCODE    int null,
    constraint PRODUCTS_BARCODES_ID_uindex
        unique (ID),
    constraint PRODUCTS_BARCODES_PRODUCTS_ID_fk
        foreign key (ID_PRODUCT) references PRODUCTS (ID)
);

alter table PRODUCTS_BARCODES
    add primary key (ID);


